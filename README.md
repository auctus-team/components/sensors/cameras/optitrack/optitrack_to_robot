This repo propose a tool to calibrate the position of the optitrack world pose to a robot world pose in ROS.

The package uses [vrnp_client_ros](https://github.com/ros-drivers/vrpn_client_ros) to listen to the optitrack markers. It uses a CS100 calibration square for the calibration. 

# Motive side 

You can import the `square_calib.motive` motive asset to motive. It defines the cs100 rigid body with the center of the frame on the marker located at the 90° angle.

Stream using the vrpn protocol and the Y-up convention. Note the server adress in the data streaming pane


 When calibrating the world ground, put the calibration tool on the table, the z axis pointing toward the franka y axis. You may have to rotate the cs100 calibration tool to match the correct orientation (this is yet to be fixed). 

# Ros side 

## Calibration

The calibration square must be placed at a known pose relatively to the ROS world. In our case it is placed at the corner of the table. The pose of the square is entered in `config/world_to_calibration_square.yaml` 

The `optitrack_world_calib.py` scripts compares the pose of the calibration square in motive and in ROS and compute the transformation matrix to express the optitrack world pose to the ROS world pose.

You can run `roslaunch optitrack_to_robot calibrate.launch` to run the calibration. You can specify the name of the optitrack world frame, the name of the calibration square frame must be the same as the rigid body defined in motive. You can also specify the vrpn server adress.

## Publishing the optitrack_world pose relatively to the ROS world pose
The `static_tf_publisher.py` scripts load the calibrated pose and publish it to the tf server. 

You can run `roslaunch optitrack_to_robot run.launch` to use it. you can specify the vrpn server adress 
