#!/usr/bin/env python3

from scipy.spatial.transform import Rotation as R
import numpy as np
from tf2_ros import Buffer, TransformListener, TransformBroadcaster
import rospy
import time
from rospy import Time, Duration
import geometry_msgs
import yaml
import rospkg


def tf_to_H(tf):
    tf_rotation = tf.transform.rotation
    tf_translation = tf.transform.translation
    rot = R.from_quat(
        [tf_rotation.x, tf_rotation.y, tf_rotation.z, tf_rotation.w]
    ).as_matrix()
    t = np.array([[tf_translation.x], [tf_translation.y], [tf_translation.z]])
    return np.block([[rot, t], [0, 0, 0, 1]])


def loadTransformFromYaml(yaml_path):
    transform = geometry_msgs.msg.TransformStamped()
    with open(yaml_path) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
        transform.header.stamp = rospy.Time.now()
        transform.header.frame_id = data["header"]["frame_id"]
        transform.child_frame_id = data["header"]["child_frame_id"]
        transform.transform.translation.x = data["position"]["x"]
        transform.transform.translation.y = data["position"]["y"]
        transform.transform.translation.z = data["position"]["z"]
        transform.transform.rotation.x = data["orientation"]["x"]
        transform.transform.rotation.y = data["orientation"]["y"]
        transform.transform.rotation.z = data["orientation"]["z"]
        transform.transform.rotation.w = data["orientation"]["w"]
    return transform


def saveTransformToYaml(translation, rotation):
    calib_dat = dict(
        header=dict(frame_id="world", child_frame_id=optitrack_world_name),
        position=dict(
            x=float(translation[0]),
            y=float(translation[1]),
            z=float(translation[2]),
        ),
        orientation=dict(
            x=float(rotation[0]),
            y=float(rotation[1]),
            z=float(rotation[2]),
            w=float(rotation[3]),
        ),
    )

    with open(
        rospack.get_path("optitrack_to_robot")
        + "/config/world_to_world_optitrack.yaml",
        "w",
    ) as file:
        yaml.dump(calib_dat, file, default_flow_style=False)


def main():
    rospy.logdebug("Beginning calibration")

    world_optitrack_H_calibration_square = tf_to_H(
        tfBuffer.lookup_transform(
            optitrack_world_name, square_calib_tf_name, Time(0), Duration(2)
        )
    )
    rospy.logdebug("world_optitrack_H_calibration_square \n")
    rospy.logdebug(world_optitrack_H_calibration_square)
    path = (
        rospack.get_path("optitrack_to_robot")
        + "/config/world_to_calibration_square.yaml"
    )

    world_H_calibration_square = tf_to_H(loadTransformFromYaml(path))
    rospy.logdebug("world_H_calibration_square \n")
    rospy.logdebug(world_H_calibration_square)

    world_H_world_optitrack = np.dot(
        world_H_calibration_square, np.linalg.inv(world_optitrack_H_calibration_square)
    )
    rospy.logdebug("world_H_world_optitrack \n")
    rospy.logdebug(world_H_world_optitrack)
    T_final = world_H_world_optitrack[0:3, 3:4]
    rot_final = R.from_matrix(world_H_world_optitrack[0:3, 0:3]).as_quat()

    rospy.logdebug(
        str(T_final[0][0])
        + " "
        + str(T_final[1][0])
        + " "
        + str(T_final[2][0])
        + " "
        + str(rot_final[0])
        + " "
        + str(rot_final[1])
        + " "
        + str(rot_final[2])
        + " "
        + str(rot_final[3])
    )
    saveTransformToYaml(T_final, rot_final)


if __name__ == "__main__":
    try:
        rospy.init_node("optitrack_calibration", log_level=rospy.DEBUG)
        rate = rospy.Rate(100)  # 10hz
        tfBuffer = Buffer()
        tfListener = TransformListener(tfBuffer)
        rospack = rospkg.RosPack()
        optitrack_world_name = rospy.get_param(
            "~optitrack_world_name", "world_optitrack"
        )
        square_calib_tf_name = rospy.get_param("~square_calib_tf_name", "square_calib")
        print(optitrack_world_name)
        print(square_calib_tf_name)
        main()
    except rospy.ROSInterruptException:
        pass
